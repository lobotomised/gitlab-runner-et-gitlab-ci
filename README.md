# Gitlab CI pipeline pour Laravel

Fichiers de base pour la gestion des pipeline de Gitlab.

## Docker
Le Dockerfile permet de créer l'image lobotomised/gitlab-ci-runner-php7.3
````
docker build -t lobotomised/gitlab-ci-runner-php7.3:latest .
````
Le '.' étant le path vers le dossier contenant le Dockerfile

Ensuite envoyer la nouvelle image vers DockerHub
````
docker push lobotomised/gitlab-ci-runner-php7.3:latest
````

Pour gitlab.com, créer l'image avec 
````
docker build -t registry.gitlab.com/lobotomised/robin:latest .
````
et le pousser dans le registry de gitlab
````
docker push registry.gitlab.com/lobotomised/robin:latest
````

## Gitlab
Le fichier .gitlab_ci doit être placer à la racine du projet à tester. Modifier les variables mysql pour être cohérentes avec les variables du .env.example (ou .env.testing) du projet
